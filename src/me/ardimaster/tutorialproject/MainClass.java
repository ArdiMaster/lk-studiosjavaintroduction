package me.ardimaster.tutorialproject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by ArdiMaster on 14.09.2015.
 */
public class MainClass {
    public static void main(String[] args) {
        System.out.print("Please enter your name: ");
        String name = "";
        InputStreamReader inputStreamReader = new InputStreamReader(System.in);
        BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
        try {
            name = bufferedReader.readLine();
            System.out.println("Hello, " + name + "! Nice to meet you!");
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Sorry, something went wrong :(");
        }
    }
}
